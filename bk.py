import asyncio
from bleak import BleakScanner
from bleak import BleakClient

async def scan():
    devices = await BleakScanner.discover()
    for d in devices:
        print(d)

asyncio.run(scan())
print("--------------------")

address = "E8:64:BA:B6:2F:85"
async def connect(address):
    client = BleakClient(address)
    try:
        print("zer")
        await client.connect()
        print("aze")
        services = await client.get_services()
        for service in services:
            print(f"Service UUID: {service.uuid}")
            for characteristic in service.characteristics:
                print(f"  Characteristic UUID: {characteristic}")
    except Exception as e:
        print(e)
        print("ert")
    finally:
        await client.disconnect()
        
asyncio.run(connect(address))