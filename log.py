import asyncio
from bleak import BleakClient

async def notif_handler(sender, data):
    print(f"Notif reçue de {sender}: {data}")

async def start_notification(address, char_uuid):
    client = BleakClient(address)
    try:
        await client.start_notify(char_uuid, notif_handler)
        print(f"Notification démarrée sur la caractéristique {char_uuid}...")
        await asyncio.sleep(600)  # Vous pouvez ajuster la durée de notification
    except Exception as e:
        print(f"Erreur lors du démarrage de la notification : {e}")

if __name__ == "__main__":
    address = "E8:64:BA:B6:2F:85"
    char_uuid = "1b0d140a-a720-f7e9-46b6-31b601c4fca1"   #log      #1b0d1407-a720-f7e9-46b6-31b601c4fca1    ack
    
    loop = asyncio.get_event_loop()
    loop.run_until_complete(start_notification(address, char_uuid))
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    #1b0d140a-a720-f7e9-46b6-31b601c4fca1    log
    #1b0d1407-a720-f7e9-46b6-31b601c4fca1    aclk
