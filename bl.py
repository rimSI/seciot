import asyncio
from bleak import BleakScanner, BleakClient

async def scan():
    devices = await BleakScanner.discover()
    for d in devices:
        print(d)

asyncio.run(scan())
print("--------------------")

async def discover_device_services(address):
    client = BleakClient(address)
    try:
        await client.connect()
        services = await client.get_services()
        for service in services:
            print(f"Service UUID : {service.uuid}")
            for char in service.characteristics:
                print(f"  - Caractéristique UUID : {char.uuid}")
                try: 
                    value = await client.read_gatt_char(char.uuid)
                    print(f"Valeur de la caractéristique : {value}")
                except:
                    print("Valeur de caracteristique non permise")
                print("--------------------------------------------------------------")
    except Exception as e:
        print(e)
        print("ert")
    finally:
        await client.disconnect()

addresse = "E8:64:BA:B6:2F:85"
    

asyncio.run(discover_device_services(addresse))
