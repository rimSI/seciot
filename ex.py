import asyncio
from bleak import BleakScanner, BleakClient


async def main(address):
    client = BleakClient(address)
    is_connected = client.is_connected
    if is_connected:
        while True:
                print("1. Read a char")
                print("2. Write to a char")
                print("3. Read all char")
                print("4. List all files")
                print("5. Extract all Files")
                c = input("Enter your choice: ")
                
                if c == '1':
                    char_uuid = input("Enter the char UUID : ")
                    try:
                        value = await client.read_gatt_char(char_uuid)
                        print(f"Value: {value}")
                    except Exception as e:
                        print(f"Failed: {e}")
                
                elif c == '2':
                    char_uuid = input("Enter the char UUID: ")
                    value = input("Enter what to write: ")
                    try:
                        value_bytes = bytes.fromhex(value)
                        await client.write_gatt_char(char_uuid, value_bytes, response=True)
                        print("successful")
                    except Exception as e:
                        print(f"Failed: {e}")
                        
                elif c == '3':
                    for service in client.services:
                        print(f"Service: {service.uuid}")
                        for char in service.characteristics:
                            if "read" in char.properties:
                                try:
                                    value = await client.read_gatt_char(char.uuid)
                                    char_name = client.CHARACTERISTIC_NAMES.get(char.uuid, "Unknown Characteristic")
                                    print(f"\tCharacteristic {char.uuid}: {value} and name = {char_name}")
                                except Exception as e:
                                    print(f"\tFailed to read {char.uuid}: {e}")
                            
                            
    else:
            print("Failed to connect.")





