import asyncio
from bleak import BleakClient

address = "e8:64:ba:b6:2f:85"
MODEL_NBR_UUID = "1B0D1201-A720-F7E9-46B6-31B601C4FCA1"

async def main(address):
    client = BleakClient(address)
    try:
        await client.connect()
        model_number = await client.read_gatt_char(MODEL_NBR_UUID)
        print("Model Number: {0}".format("".join(map(chr, model_number))))
    except Exception as e:
        print(e)
    finally:
        await client.disconnect()

asyncio.run(main(address))