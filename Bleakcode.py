import asyncio
from bleak import BleakScanner
from bleak import BleakClient
from bleak.backends.characteristic import BleakGATTCharacteristic

async def scan():
    devices = await BleakScanner.discover()
    for d in devices:
        print(d)

asyncio.run(scan())
print("--------------------")

address = "E8:64:BA:B6:2F:85"
MODEL_NBR_UUID = "1B0D1201-A720-F7E9-46B6-31B601C4FCA1"
handles = []
characteristics = []

def callback(sender: BleakGATTCharacteristic, data: bytearray):
    print(f"{sender}: {data}")
    
async def connect(address, timeout=30.0):
    async with BleakClient(address) as client:
        try:
            services = client.services
            for s in services:
                print(s)
                for c in s.characteristics:
                    print(c)
                    characteristics.append(c)
                    handles.append(c.handle)
                print("\n")
           # model_number = await client.read_gatt_char(MODEL_NBR_UUID)
            #print(model_number)
            #fichier = await client.start_notify(66, callback)
        except Exception as e:
            print(e)
            print("ert")
        
asyncio.run(connect(address))

print(handles)

async def connect(address):
    client = BleakClient(address)
    try:
        print("zer")
        await client.connect()
        print("aze")
        services = await client.get_services()
        for service in services:
            print(f"Service UUID: {service.uuid}")
            for characteristic in service.characteristics:
                print(f"  Characteristic UUID: {characteristic}")
    except Exception as e:
        print(e)
        print("ert")
    finally:
        await client.disconnect()