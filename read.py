import asyncio
from bleak import BleakClient

address = "E8:64:BA:B6:2F:85" 
handle = 66  # Handle de la caractéristique depuis laquelle lire

async def read_and_retrieve_files(add, han, timeout=30):
    async with BleakClient(add) as client:
        value = await client.read_gatt_char(han)
        
        with open("file.txt", "w") as file:
            file.write(value.decode("utf-8")) 
        
        print("Done!")

async def main():
    await read_and_retrieve_files(address, handle)

if __name__ == "__main__":
    asyncio.run(main())
